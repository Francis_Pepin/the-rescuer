This game was made by Francis Pepin, Thomas Charbonneau, Alexandre Trépanier and Rémi Lepage as part of a class at Polytechnique Montréal. The objective was to produce a small 2.5D platformer prototype with different mechanics of our choosing in a bit less than 2 months. Apart from the astronaut character, the enemies and some sound effects, all of the assets and the soundtrack have been created by Francis Pepin using Blender and Ableton.

You can also launch the game from the "The Rescuer" folder by running the .exe file named the same.

Controls :  
Move : WASD  
Jump : Spacebar (Hold for modular jump)  
Interact: E  
Left Click : Grapple to rocks  
Right Click : Grapple attack  
Shift : Jetpack  

Link to playthrough video:
[![The rescuer - Full Playthrough](https://i.ytimg.com/vi/M7yOLr16A_4/maxresdefault.jpg)](https://www.youtube.com/watch?v=M7yOLr16A_4)
