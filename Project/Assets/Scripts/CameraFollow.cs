using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private Vector3 offset = new Vector3(0f, 2f, -14f);
    [SerializeField] private float followFactor = 3f;

    private float time = 0f;

    private Transform player;
    private Vector3 initialPosition;
    private Vector3 previousPosition;

    void Start()
    {
        var forcePlayerController = FindObjectOfType<ForcePlayerController>();
        if (forcePlayerController)
        {
            player = forcePlayerController.transform;
        }

        initialPosition = transform.position;
        previousPosition = transform.position;
    }

    void Update()
    {
        if (!player)
        {
            return;
        }

        time += Time.deltaTime;
        var rotation = Quaternion.FromToRotation(Vector3.up, player.up);
        float radianAngle = 2f * Mathf.PI / 360f * rotation.eulerAngles.z;

        var angledDirection = new Vector3(-Mathf.Sin(radianAngle), Mathf.Cos(radianAngle), 0f) * 3;

        var targetPosition = player.transform.position + offset + angledDirection;
        transform.position = Vector3.Lerp(previousPosition, targetPosition, Time.deltaTime * followFactor);
        previousPosition = transform.position;
    }
}
