using UnityEngine;
using UnityEngine.UI;

public class ProgressionBar : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform progressionBarImage;
    [SerializeField] private Transform progressionBarBorder;
    [SerializeField] private Text text;

    private float progressionBardBorderRatio = 1.0f;

    public void UpdateValue(float ratio)
    {
        progressionBarImage.localScale = new Vector2(ratio * progressionBardBorderRatio, 1f);
        text.text = (Mathf.Round(ratio * progressionBardBorderRatio * 100f)).ToString();
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    public void UpdateMaxRatio(float ratioModifier)
    {
        progressionBardBorderRatio *= ratioModifier;
        progressionBarBorder.localScale = new Vector2(progressionBardBorderRatio, 1f);
    }
}
