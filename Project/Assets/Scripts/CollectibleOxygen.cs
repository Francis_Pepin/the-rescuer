using UnityEngine;

public class CollectibleOxygen : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float additionnalOxygen = 0.1f;

    private void OnTriggerEnter(Collider other)
    {
        Oxygen oxygen = other.GetComponent<Oxygen>();
        if (oxygen != null)
        {
            oxygen.IncreaseCurrentOxygen(additionnalOxygen);
            gameObject.SetActive(false);
        }
    }
}
