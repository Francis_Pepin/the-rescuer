using UnityEngine;

public class BodyGravity : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float gravity = 10f;

    private bool isInside = false;
    private float movementTimer = 0f;

    private Rigidbody rb;
    private GameObject bodyObject;
    private ForcePlayerController controller;
    private Vector3 normalVector;
    private Quaternion previousRotation;
    private Quaternion targetRotation;

    void FixedUpdate()
    {
        if (rb)
        {
            if (controller.IsBeingTransported)
            {
                return;
            }

            targetRotation = GetTargetRotation(isInside, out normalVector);
            previousRotation = rb.transform.rotation;

            // Interpolate to desired rotation or set to desired rotation if close to it
            if (Quaternion.Angle(rb.transform.rotation, targetRotation) < 5f)
            {
                rb.transform.rotation = targetRotation;
            }
            else
            {
                rb.transform.rotation = Quaternion.Lerp(previousRotation, targetRotation, Time.deltaTime * 10f);
            }

            if (isInside)
            {
                // Apply gravity with normal vector
                float gravityFactor = 100f;
                rb.AddForce(-normalVector * gravity * Time.deltaTime * gravityFactor);
            }
            else if (!isInside && rb.transform.rotation == targetRotation)
            {
                // Get rid of the rigidbody if the player's rotation is in line with the flat surface'
                controller.CanMove = true;
                rb.useGravity = !controller.IsInAtmosphere;
                rb = null;
                bodyObject = null;
            }
        }
    }

    // To avoid being stuck forever without moving in case of a problem
    private void Update()
    {
        if (controller != null && !controller.CanMove)
        {
            movementTimer += Time.deltaTime;
            if (movementTimer > 5f)
            {
                if (targetRotation != null && rb != null)
                {
                    rb.transform.rotation = targetRotation;
                    rb.useGravity = !controller.IsInAtmosphere;
                }
                controller.CanMove = true;
                rb = null;
                bodyObject = null;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody newBody = other.GetComponent<Rigidbody>();
        if (newBody && other.GetType() == typeof(SphereCollider))
        {
            rb = newBody;
            bodyObject = other.gameObject;
            controller = bodyObject.GetComponent<ForcePlayerController>();
            previousRotation = rb.transform.rotation;

            // Remove default gravity since the new one is applied according to the astral body
            rb.useGravity = false;
            isInside = true;
            controller.IsInAtmosphere = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Rigidbody exitingBody = other.GetComponent<Rigidbody>();
        if (exitingBody && other.GetType() == typeof(SphereCollider))
        {
            // Determine the normal rotation to reach outside of the astral body
            targetRotation = Quaternion.Euler(new Vector3(0f, 90f, 0f));
            previousRotation = rb.transform.rotation;
            controller.CanMove = false;
            movementTimer = 0f;
            isInside = false;
            controller.IsInAtmosphere = false;
        }
    }

    private Quaternion GetTargetRotation(bool isInside, out Vector3 normalVector)
    {
        var rotation = Quaternion.identity;
        if (isInside)
        {
            // Rotate player according to normal vector with sphere
            Vector3 feetPosition = bodyObject.transform.Find("GroundCheck").transform.position;
            normalVector = (feetPosition - transform.position);
            normalVector.Normalize();

            rotation = Quaternion.FromToRotation(rb.transform.up, normalVector) * rb.transform.rotation;
        }
        else
        {
            // Normal rotation on flat surface
            var currentRotationY = rb.transform.rotation.eulerAngles.y;
            var closestRotationY = Mathf.Abs(90f - currentRotationY) < Mathf.Abs(270f - currentRotationY) ? 90f : 270f;
            rotation = Quaternion.Euler(new Vector3(0f, closestRotationY, 0f));
            normalVector = new Vector3(0f, 1f, 0f);
        }

        return rotation;
    }
}
