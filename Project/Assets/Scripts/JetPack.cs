using UnityEngine;

public class JetPack : MonoBehaviour
{
    public bool IsUsingJetpack = false;
    public bool IsJetpackUnlocked = true;
    public ProgressionBar ProgressBar
    {
        get => fuelBar;
    }

    [Header("Values")]
    [SerializeField] private float jetpackForce = 10f;
    [SerializeField] private float initialJetpackFuel = 5f;
    [SerializeField] private float jetpackFuelRecoverySpeed = 1f;
    [SerializeField] private float jetpackEmptyRecoverySpeed = 0.3f;
    [SerializeField] private bool unlimitedJetpackFuel = false;

    [Header("References")]
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Transform astronautUpAxis;
    [SerializeField] private ProgressionBar fuelBar;
    [SerializeField] private ParticleSystem fireTrail;
    [SerializeField] private ParticleSystem fireParticules;
    [SerializeField] private ParticleSystem smokeTrail;

    private float currentJetpackFuel;
    private float recoveryTime;
    private ForcePlayerController playerController;

    void Start()
    {
        currentJetpackFuel = initialJetpackFuel;
        SetActiveParticleSystems(false);
        if (fuelBar != null)
        {
            fuelBar.SetActive(!unlimitedJetpackFuel && IsJetpackUnlocked);
        }
        recoveryTime = jetpackEmptyRecoverySpeed;

        playerController = rb.GetComponent<ForcePlayerController>();
    }

    public void UpdateJetPack(bool canMove, bool isGrounded)
    {
        recoveryTime += Time.deltaTime;

        if (!IsJetpackUnlocked || playerController.IsBeingTransported)
        {
            IsUsingJetpack = false;
            return;
        }

        if (isGrounded)
        {
            currentJetpackFuel = Mathf.Min(currentJetpackFuel + Time.deltaTime * jetpackFuelRecoverySpeed, initialJetpackFuel);
        }

        if (Input.GetButton("JetPack") && canMove && recoveryTime > jetpackEmptyRecoverySpeed && (currentJetpackFuel > 0 || unlimitedJetpackFuel))
        {
            IsUsingJetpack = true;
            Vector3 locVel = astronautUpAxis.InverseTransformDirection(rb.velocity);
            float effectiveForce = (jetpackForce - locVel.y) * 2f;
            var localSpeed = new Vector3(0f, effectiveForce, 0f);
            Vector3 worldSpeed = astronautUpAxis.TransformDirection(localSpeed);

            rb.AddForce(worldSpeed * Time.deltaTime, ForceMode.Impulse);
            currentJetpackFuel = Mathf.Max(0f, currentJetpackFuel - Time.deltaTime);

            if (currentJetpackFuel == 0)
            {
                recoveryTime = 0;
            }
            SetActiveParticleSystems(true);
        }
        else
        {
            IsUsingJetpack = false;
            SetActiveParticleSystems(false);
        }

        if (fuelBar != null)
        {
            fuelBar.UpdateValue(currentJetpackFuel / initialJetpackFuel);
        }
    }

    public void IncreaseFuelLimit(float additionnalFuel)
    {
        fuelBar.UpdateMaxRatio((initialJetpackFuel + additionnalFuel) / initialJetpackFuel);
        initialJetpackFuel += additionnalFuel;
    }

    private void SetActiveParticleSystems(bool isActive)
    {
        var fireTrailEmission = fireTrail.emission;
        var smokeTrailEmission = smokeTrail.emission;
        var fireParticulesEmission = fireParticules.emission;
        fireTrailEmission.enabled = isActive;
        smokeTrailEmission.enabled = isActive;
        fireParticulesEmission.enabled = isActive;
    }
}
