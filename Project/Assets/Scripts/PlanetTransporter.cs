using Unity.VisualScripting;
using UnityEngine;

public class PlanetTransporter : MonoBehaviour
{
    public bool isTransportingPlayer = false;
    public bool isOnCooldown = false;

    [SerializeField] private Transform targetTransporterTransform;
    [SerializeField] private Transform insideIcosphere, middleIcosphere, outsideIcosphere;
    [SerializeField] private GrapplingHook grapplingHook;
    [SerializeField] private HookAttack hookAttack;
    [SerializeField] private float rotateSpeed = 50;
    [SerializeField] private AudioClip launchSound;
    [SerializeField] private AudioClip arrivedSound;

    private Vector3 translateVelocity = Vector3.zero;

    private float cooldown;
    private Light sphereLight;
    private Light targetSphereLight;
    private Rigidbody rb;
    private AudioSource audioSource;
    private ForcePlayerController controller;
    private PlanetTransporter targetTransporter;

    private bool CanStartTransporting
    {
        get => !(isOnCooldown || isTransportingPlayer) &&
            !(targetTransporter.isTransportingPlayer || targetTransporter.isOnCooldown) &&
            !grapplingHook.IsGrappling && !hookAttack.IsActive;
    }

    private void Start()
    {
        targetTransporter = targetTransporterTransform.gameObject.GetComponent<PlanetTransporter>();
        sphereLight = GetComponent<Light>();
        audioSource = GetComponent<AudioSource>();
        targetSphereLight = targetTransporterTransform.GetComponent<Light>();

        var lineRenderer = GetComponent<LineRenderer>();
        var padding = (targetTransporterTransform.position - transform.position).normalized;
        lineRenderer.SetPosition(0, transform.position - padding);
        lineRenderer.SetPosition(1, targetTransporterTransform.position + padding);
    }

    private void FixedUpdate()
    {
        var adjustedSpeed = rotateSpeed * Time.deltaTime;
        insideIcosphere.Rotate(0f, 0f, adjustedSpeed);
        middleIcosphere.Rotate(0f, adjustedSpeed, 0f);
        outsideIcosphere.Rotate(adjustedSpeed, 0f, 0f);
    }

    private void Update()
    {
        if (!rb)
        {
            return;
        }

        var isTargetActive = targetTransporter.isTransportingPlayer || targetTransporter.isOnCooldown;
        if (isTransportingPlayer && !isTargetActive)
        {
            rb.useGravity = false;
            controller.CanMove = false;
            ChangeLightColors(Color.red);
            controller.IsBeingTransported = true;
            grapplingHook.IsGrappleAllowed = false;
            rb.transform.position = Vector3.SmoothDamp(
                rb.transform.position,
                targetTransporterTransform.position,
                ref translateVelocity,
                0.1f,
                10f,
                Time.deltaTime
            );
        }

        if (Vector3.Distance(rb.transform.position, targetTransporterTransform.position) < 0.05f)
        {
            cooldown = 0f;
            isTransportingPlayer = false;
            controller.IsBeingTransported = false;
            grapplingHook.IsGrappleAllowed = true;
            rb.useGravity = !controller.IsInAtmosphere;
            rb.transform.Find("TransportSphere").gameObject.SetActive(false);
            controller.CanMove = true;
            if (!isOnCooldown)
            {
                audioSource.PlayOneShot(arrivedSound);
            }
            isOnCooldown = true;
            ChangeLightColors(Color.yellow);
        }

        if (cooldown > 3f && isOnCooldown)
        {
            isOnCooldown = false;
            ChangeLightColors(Color.white);
        }

        cooldown += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody newBody = other.GetComponent<Rigidbody>();
        if (newBody && other.GetType() == typeof(SphereCollider) && CanStartTransporting)
        {
            rb = newBody;

            // Remove default gravity since the new one is applied according to the astral body
            rb.useGravity = false;
            rb.velocity = new Vector3(0f, 0f, 0f);
            controller = other.gameObject.GetComponent<ForcePlayerController>();
            controller.CanMove = false;
            isTransportingPlayer = true;
            audioSource.PlayOneShot(launchSound);
            rb.transform.Find("TransportSphere").gameObject.SetActive(true);
        }
    }

    private void ChangeLightColors(Color color)
    {
        sphereLight.color = color;
        targetSphereLight.color = color;
    }
}
