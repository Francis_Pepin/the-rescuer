using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    [Header("Values")]
    public bool IsGrappleAllowed = true;
    public bool IsGrappleUnlocked = true;
    public bool IsGrappling
    {
        get => isGrappling;
    }

    [SerializeField] private float maxDistance = 6f;
    [SerializeField] private float swingStrength = 15f;
    [SerializeField] private float enemyKillTime = 1f;

    [Header("References")]
    [SerializeField] private Transform player;
    [SerializeField] private LayerMask whatCanBeGrappled;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private AudioClip ropeSound;
    [SerializeField] private HookAttack hookAttack;

    private bool isGrappling = false;
    private bool wasGrappling = false;
    private float timeSinceGrapple = 0f;

    private Vector3 target;
    private Vector3 normal;
    private Collider targetCollider;
    private Transform hook;
    private ForcePlayerController controller;
    private Rigidbody rb;
    private Camera cam;
    private LineRenderer line;
    private AudioSource audioSource;

    private Vector3 Origin
    {
        get => player.position + player.transform.up * 0.3f;
    }

    public void Reset()
    {
        isGrappling = false;
        target = Vector3.zero;
    }

    private void Start()
    {
        controller = player.GetComponent<ForcePlayerController>();
        rb = player.GetComponent<Rigidbody>();
        cam = FindObjectOfType<Camera>();
        audioSource = GetComponent<AudioSource>();
        line = GetComponent<LineRenderer>();

        hook = transform.Find("Hook");
        hook.gameObject.SetActive(false);
    }

    private void Update()
    {
        // Do nothing if grappling is disabled
        if (!IsGrappleAllowed || !IsGrappleUnlocked || hookAttack.IsActive)
        {
            return;
        }

        RaycastHit hit;
        if (!isGrappling)
        {
            SetGrappleActive(false);
            timeSinceGrapple = 0;

            if (Input.GetButton("Fire1"))
            {
                Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(
                    new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.transform.position.z)
                );
                mouseWorldPosition = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, 0f);

                bool canGrapple = Physics.Raycast(Origin, (mouseWorldPosition - Origin), out hit, maxDistance, whatCanBeGrappled);

                RaycastHit groundHits;
                canGrapple = canGrapple && !Physics.Raycast(Origin, (mouseWorldPosition - Origin), out groundHits, maxDistance, whatIsGround);
                if (canGrapple && target == Vector3.zero)
                {
                    target = hit.point;
                    normal = hit.normal;
                    targetCollider = hit.collider;
                    isGrappling = true;
                    audioSource.PlayOneShot(ropeSound);
                }
            }
        }

        var isButtonUp = (Input.GetButtonUp("Fire1"));
        if (isButtonUp)
        {
            isGrappling = false;
            target = Vector3.zero;
        }

        Grapple();
    }

    private void Grapple()
    {
        if (isGrappling)
        {
            wasGrappling = true;
            timeSinceGrapple += Time.deltaTime;
            var direction = (target - Origin).normalized;
            if (targetCollider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                if (timeSinceGrapple > enemyKillTime)
                {
                    // Kill enemy
                    targetCollider.gameObject.SetActive(false);
                    isGrappling = false;
                }
                DrawRope(direction);
            }
            else
            {
                float swingFactor = 100f;
                rb.AddForce(direction * swingStrength * Time.deltaTime * swingFactor);
                DrawRope(direction);
            }

            controller.CanMove = false;
        }
        else
        {
            if (wasGrappling)
            {
                controller.CanMove = true;
            }
            wasGrappling = false;
        }
    }

    private void DrawRope(Vector3 direction)
    {
        SetGrappleActive(true);

        line.SetPosition(0, Origin);
        line.SetPosition(1, target);

        float angle = Vector3.Angle(Vector3.up, new Vector3(normal.x, -normal.y, 0f));
        if (normal.x < 0f)
        {
            angle = -angle + 360f;
        }
        hook.eulerAngles = new Vector3(0f, 0f, angle);
        hook.position = target;
    }

    private void SetGrappleActive(bool isActive)
    {
        hook.gameObject.SetActive(isActive);
        line.positionCount = isActive ? 2 : 0;
    }
}
