using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip initialSoundtrack;

    private AudioClip nextSong;

    public float Volume
    {
        get => audioSource.volume;
    }

    public void SetVolume(float ratio)
    {
        audioSource.volume = ratio;
    }

    public void StopMusic()
    {
        nextSong = null;
        audioSource.Stop();
    }

    public void PlayInitialSoundtrack()
    {
        audioSource.PlayOneShot(initialSoundtrack);
        nextSong = initialSoundtrack;
    }

    public void Play(AudioClip audio)
    {
        audioSource.PlayOneShot(audio);
        nextSong = audio;
    }

    private void Update()
    {
        if (!audioSource.isPlaying && nextSong != null)
        {
            Play(nextSong);
        }
    }
}
