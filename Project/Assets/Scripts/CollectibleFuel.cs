using UnityEngine;

public class CollectibleFuel : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float additionnalFuel = 1f;

    private void OnTriggerEnter(Collider other)
    {
        JetPack jetpack = other.GetComponent<JetPack>();
        if (jetpack != null)
        {
            jetpack.IncreaseFuelLimit(additionnalFuel);
            gameObject.SetActive(false);
        }
    }
}
