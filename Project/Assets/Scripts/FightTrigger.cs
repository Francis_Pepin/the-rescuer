using UnityEngine;

public class FightTrigger : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private BossEnemy boss;
    [SerializeField] private Camera cam;

    [Header("Values")]
    [SerializeField] private float targetSize = 6f;

    private float currentSize = -1f;

    private void OnTriggerEnter(Collider other)
    {
        var controller = other.GetComponent<ForcePlayerController>();
        if (controller != null)
        {
            boss.CanStartFight = true;
            if (currentSize < 0f)
            {
                currentSize = cam.orthographicSize;
            }
        }
    }

    private void Update()
    {
        if (currentSize < targetSize && currentSize > 0f)
        {
            currentSize = Mathf.Lerp(currentSize, targetSize + 0.1f, Time.deltaTime * 2f);
            cam.orthographicSize = currentSize;
        }

        if (currentSize > targetSize)
        {
            currentSize = -1f;
        }
    }
}
