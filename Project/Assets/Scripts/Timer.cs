using System;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float TimeValue
    {
        get => timeValue;
    }

    [Header("References")]
    [SerializeField] private Text text;

    private bool shouldUpdateTime = true;
    private float timeValue;

    void Start()
    {
        text.text = "00:00.000";
    }

    void Update()
    {
        if (shouldUpdateTime)
        {
            timeValue += Time.deltaTime;
            TimeSpan timeSpan = TimeSpan.FromSeconds(timeValue);
            text.text = $"{timeSpan.Minutes.ToString("00")}:{timeSpan.Seconds.ToString("00")}.{timeSpan.Milliseconds.ToString("000")}";
        }
    }

    public void StopTime()
    {
        shouldUpdateTime = false;
    }
}
