using UnityEngine;

public class Spikes : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float damage = 0.2f;
    [SerializeField] private float knockbackSpeed = 8f;

    [Header("References")]
    [SerializeField] private AudioClip hitPlayer;
    [SerializeField] private Transform knockbackOrigin;

    private float timeSinceHit = 1f;
    private bool hasBeenHit = false;
    private AudioSource audioSource;
    private ForcePlayerController playerController;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        timeSinceHit += Time.deltaTime;
        if (hasBeenHit && timeSinceHit > 1f)
        {
            playerController.CanMove = true;
            hasBeenHit = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (timeSinceHit < 1f)
        {
            return;
        }

        var oxygen = other.gameObject.GetComponent<Oxygen>();
        if (oxygen)
        {
            oxygen.DecreaseOxygen(damage);
            audioSource.PlayOneShot(hitPlayer);
            timeSinceHit = 0f;
            var rb = other.GetComponent<Rigidbody>();
            playerController = other.gameObject.GetComponent<ForcePlayerController>();
            if (rb && playerController)
            {
                hasBeenHit = true;
                playerController.CanMove = false;
                Vector3 origin = knockbackOrigin ? knockbackOrigin.position : transform.position;
                Vector3 knockbackDirection = Vector3.Normalize(rb.position - origin);
                rb.velocity = knockbackDirection * knockbackSpeed;
            }
        }
    }
}
