using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    static bool IsPaused = false;
    [SerializeField] private MusicManager musicManager;
    [SerializeField] private Credits credits;
    [SerializeField] private Animator fadeAnim;
    [SerializeField] private Animator[] buttonAnims;

    [Header("References")]
    [SerializeField] private GameObject pauseMenuUI;

    private float fadeLength = 1000f;
    private float fadeTimer = 0f;
    private float initialVolume = 1f;
    private float volume = 1f;
    private bool isInTransition = false;

    private bool CanPause
    {
        get => !credits.isInPause && !credits.isInTransition;
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        IsPaused = true;
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        IsPaused = false;
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        fadeAnim.SetTrigger("FadeToBlack");
        foreach (var buttonAnim in buttonAnims)
        {
            buttonAnim.SetTrigger("FadeButton");
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (Input.GetButtonDown("PauseMenu") && CanPause)
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        if (pauseMenuUI.activeSelf && fadeAnim.GetCurrentAnimatorStateInfo(0).IsName("Black Fade Credits"))
        {
            fadeLength = fadeAnim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
            initialVolume = musicManager.Volume;
            volume = musicManager.Volume;
            isInTransition = true;
        }

        if (isInTransition)
        {
            fadeTimer += Time.deltaTime;
            volume = initialVolume - (fadeTimer / fadeLength * initialVolume);
            musicManager.SetVolume(volume);
        }

        if (fadeTimer > fadeLength)
        {
            SceneManager.LoadScene(0);
        }
    }
}
