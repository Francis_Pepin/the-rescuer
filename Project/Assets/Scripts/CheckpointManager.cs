using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform respawnPoint;
    [SerializeField] private Camera cam;
    [SerializeField] private MusicManager musicManager;

    private Color currentColor = Color.black;
    private Color targetColor = Color.black;

    private float maxVolume = 1f;
    private float volumeTimer = 0f;
    private float fadeTimer = 0f;
    private bool hasSwitchedSongs = false;
    private string previousSongName = "";
    private AudioClip newSong;

    public void OnChildTriggerEnter(Transform checkpoint)
    {
        respawnPoint.position = checkpoint.position;
    }

    public void ChangeBackgroundColor(Color color)
    {
        currentColor = cam.backgroundColor;
        targetColor = color;
    }

    public void ChangeMusic(AudioClip audioClip)
    {
        if (audioClip.name != previousSongName)
        {
            newSong = audioClip;
            volumeTimer = 2f;
            hasSwitchedSongs = false;
            previousSongName = audioClip.name;
        }
    }

    public void FadeAudio()
    {
        fadeTimer = 1f;
        previousSongName = "";
    }

    private void Awake()
    {
        maxVolume = musicManager.Volume;
    }

    private void Update()
    {
        if (currentColor != targetColor)
        {
            cam.backgroundColor = Color.Lerp(currentColor, targetColor, Time.deltaTime * 1f);
            currentColor = cam.backgroundColor;
        }

        if (volumeTimer > 0f)
        {
            if (volumeTimer > 1f)
            {
                var volume = maxVolume - (maxVolume * (1 - (volumeTimer - 1)));
                musicManager.SetVolume(volume);
            }
            else
            {
                if (!hasSwitchedSongs)
                {
                    musicManager.StopMusic();
                    musicManager.Play(newSong);
                    hasSwitchedSongs = true;
                }

                var volume = maxVolume * (1 - volumeTimer);
                musicManager.SetVolume(volume);
            }
            volumeTimer -= Time.deltaTime;

            if (volumeTimer <= 0f)
            {
                musicManager.SetVolume(maxVolume);
            }
        }

        if (fadeTimer > 0f)
        {
            musicManager.SetVolume(fadeTimer * maxVolume);
            fadeTimer -= Time.deltaTime;
            if (fadeTimer <= 0f)
            {
                musicManager.StopMusic();
                musicManager.SetVolume(maxVolume);
            }
        }
    }
}
