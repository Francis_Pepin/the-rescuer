using UnityEngine;

public class Oxygen : MonoBehaviour
{
    public float OxygenValue
    {
        get => oxygenValue;
    }

    [Header("Values")]
    public float linearOxygenDecreasingRatio = 0.01f;

    [Header("References")]
    [SerializeField] private ProgressionBar oxygenBar;
    [SerializeField] private RespawnManager respawnManager;
    [SerializeField] private GameObject respawnPoint;

    private float oxygenValue;
    private Rigidbody rb;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        oxygenValue = 1;
        if (oxygenBar != null)
        {
            oxygenBar.SetActive(true);
        }
    }

    void Update()
    {
        if (oxygenBar != null)
        {
            DecreaseOxygen(linearOxygenDecreasingRatio * Time.deltaTime);
            oxygenBar.UpdateValue(oxygenValue);
        }

        if (Mathf.Round(oxygenValue * 100f) <= 0)
        {
            rb.velocity = Vector3.zero;
            oxygenValue = 1;
            respawnManager.HandleRespawn();
        }
    }

    public void DecreaseOxygen(float rate)
    {
        oxygenValue -= rate;
        if (oxygenValue < 0)
        {
            oxygenValue = 0;
        }
    }

    public void IncreaseCurrentOxygen(float additionnalOxygen)
    {
        oxygenValue += additionnalOxygen;

        if (oxygenValue > 1)
        {
            oxygenValue = 1;
        }

    }
}
