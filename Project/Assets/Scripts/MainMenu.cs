using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private MusicManager musicManager;
    [SerializeField] private Animator fadeAnim;
    [SerializeField] private Animator[] textAnims;

    [SerializeField] private Toggle musicToggle;

    private float transitionLength = 0f;
    private float transitionTime = 0f;
    private float initialVolume = 1f;
    private float volume = 1f;

    public void NextScene()
    {
        fadeAnim.SetTrigger("NextScene");
        foreach (var anim in textAnims)
        {
            anim.SetTrigger("NextScene");
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void Start()
    {
        musicManager.PlayInitialSoundtrack();
        if(musicManager.Volume == 0)
        {
            musicToggle.isOn = false;
        }
    }

    public void ToggleMusicVolume(bool audioOn)
    {
        if (audioOn)
        {
            musicManager.SetVolume(1);
        }
        else
        {
            musicManager.SetVolume(0);
        }
    }

    private void Update()
    {
        if (fadeAnim.GetCurrentAnimatorStateInfo(0).IsName("Fade To Black"))
        {
            transitionLength = fadeAnim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
            initialVolume = musicManager.Volume;
            volume = musicManager.Volume;
        }

        if (transitionLength > 0f)
        {
            volume = initialVolume - ((transitionTime / transitionLength) * initialVolume * 3f / 4f);
            musicManager.SetVolume(volume);
            transitionTime += Time.deltaTime;
        }

        if (volume <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
