using UnityEngine;

public class HookAttack : MonoBehaviour
{
    public bool IsActive
    {
        get => isAttacking || isRetrieving;
    }

    public bool IsUnlocked = true;

    [SerializeField] private float maxDistance = 6f;
    [SerializeField] private float timeForFullExtension = 0.5f;

    [Header("References")]
    [SerializeField] private Transform player;
    [SerializeField] private GrapplingHook grapplingHook;
    [SerializeField] private AudioClip[] grappleHits;
    [SerializeField] private AudioClip spikePoofHit;
    [SerializeField] private LayerMask whatCanBeHit;

    private bool isAttacking = false;
    private bool isRetrieving = false;
    private float timeSinceAttack = 0.5f;
    private float timeFromStartOfAttack = 0f;

    private Vector3 direction;
    private Vector3 endPoint;
    private Transform hook;
    private CapsuleCollider hitbox;
    private ForcePlayerController controller;
    private AudioSource audioSource;
    private LineRenderer line;
    private Camera cam;

    private Vector3 Origin
    {
        get => player.position + player.transform.up * 0.3f;
    }

    private bool CanAttack
    {
        get => !grapplingHook.IsGrappling && controller.CanMove && timeSinceAttack > timeForFullExtension;
    }

    void Start()
    {
        controller = player.GetComponent<ForcePlayerController>();
        cam = FindObjectOfType<Camera>();
        line = GetComponent<LineRenderer>();
        audioSource = GetComponent<AudioSource>();
        hitbox = GetComponent<CapsuleCollider>();

        endPoint = Origin;

        hook = transform.Find("Hook");
        hook.gameObject.SetActive(false);
        hitbox.enabled = false;
        direction = new Vector3(0f, 1f, 0f);
    }

    void Update()
    {
        if (!IsUnlocked)
        {
            return;
        }

        hook.position = Origin + direction * maxDistance * (timeFromStartOfAttack / timeForFullExtension);
        hook.rotation = Quaternion.LookRotation(direction);
        hook.Rotate(new Vector3(90f, 0f, 0f));

        line.SetPosition(0, Origin);
        line.SetPosition(1, hook.position);

        timeSinceAttack += Time.deltaTime;
        if (!isAttacking && !isRetrieving)
        {
            if (Input.GetButtonDown("Fire2") && CanAttack)
            {
                hook.gameObject.SetActive(true);
                hitbox.enabled = true;
                Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(
                    new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.transform.position.z)
                );
                mouseWorldPosition = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, 0f);
                direction = (mouseWorldPosition - Origin).normalized;
                isAttacking = true;
                timeFromStartOfAttack = 0f;
            }
        }

        // Keep moving attack if button is held
        if (isAttacking)
        {
            timeFromStartOfAttack += Time.deltaTime;
        }

        // Start retrieving for the same amount of time as the throw
        if (isRetrieving || (timeFromStartOfAttack >= timeForFullExtension && isAttacking))
        {
            timeFromStartOfAttack -= Time.deltaTime;
            isAttacking = false;
            hitbox.enabled = false;
            isRetrieving = true;
        }

        var height = 0.065f;
        var radius = 0.017f;
        var top = hook.position + new Vector3(0f, 0.022f + (height / 2f), 0f);
        var bottom = hook.position + new Vector3(0f, 0.022f - (height / 2f), 0f);

        var hits = Physics.CapsuleCastAll(top, bottom, radius, direction, height, whatCanBeHit);
        if (hits.Length > 0)
        {
            foreach (var hit in hits)
            {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
                {
                    HitEnemy(hit.collider);
                    if (!isRetrieving)
                    {
                        audioSource.PlayOneShot(spikePoofHit);
                    }
                }
                else if (!isRetrieving && hook.gameObject.activeSelf)
                {
                    AudioClip clip = grappleHits[Random.Range(0, grappleHits.Length)];
                    audioSource.PlayOneShot(clip);
                }
            }

            isAttacking = false;
            isRetrieving = true;
            hitbox.enabled = false;
        }

        // Reset state
        if (timeFromStartOfAttack <= 0f && isRetrieving)
        {
            timeSinceAttack = 0;
            hook.position = Origin;
            hook.gameObject.SetActive(false);
            isAttacking = false;
            isRetrieving = false;
            timeFromStartOfAttack = 0f;
        }
    }

    private void HitEnemy(Collider collider)
    {
        var enemy = collider.gameObject.GetComponent<Enemy>();
        if (enemy == null)
        {
            enemy = collider.gameObject.GetComponentInParent<Enemy>();
        }
        enemy.GetHit();
        return;
    }
}
