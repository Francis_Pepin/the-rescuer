using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    public bool isInTransition = false;
    public bool isInPause = false;
 
    [SerializeField] private Animator fadeAnim;
    [SerializeField] private Animator textAnim;
    [SerializeField] private Transform timer;
    [SerializeField] private ForcePlayerController playerController;

    private float fadeLength = 0f;
    private float textLength = 0f;
    private float transitionTime = 0f;
    private float pauseTime = 0f;

    private void Update()
    {

        if (Vector3.Distance(playerController.transform.position, transform.position) < 1f && Input.GetButtonDown("Interact"))
        {
            timer.GetComponent<Timer>().StopTime();
            playerController.CanMove = false;
            isInPause = true;
        }

        if (isInTransition)
        {
            transitionTime += Time.deltaTime;
        }
        else if (isInPause)
        {
            pauseTime += Time.deltaTime;
        }

        if (fadeAnim.GetCurrentAnimatorStateInfo(0).IsName("Black Fade Credits"))
        {
            fadeLength = fadeAnim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
        }

        if (textAnim.GetCurrentAnimatorStateInfo(0).IsName("Text Credits"))
        {
            textLength = textAnim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
        }

        if (isInPause && pauseTime > 2f)
        {
            fadeAnim.SetTrigger("FadeToBlack");
            isInPause = false;
            isInTransition = true;
        }

        if (transitionTime > fadeLength)
        {
            textAnim.SetTrigger("ShowText");
        }

        if (transitionTime > fadeLength + textLength + 1f)
        {
            // Return to main menu
            SceneManager.LoadScene(0);
        }
    }
}
