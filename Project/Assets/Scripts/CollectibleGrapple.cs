using UnityEngine;

public class CollectibleGrapple : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var hook = other.transform.parent.GetComponentInChildren<GrapplingHook>();
        var attack = other.transform.parent.GetComponentInChildren<HookAttack>();
        if (hook != null && attack != null)
        {
            hook.IsGrappleUnlocked = true;
            attack.IsUnlocked = true;
            gameObject.SetActive(false);
        }
    }
}
