using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class BossEnemy : Enemy
{
    public bool CanStartFight = false;

    [Header("Values")]
    [SerializeField] private float runSpeed = 3f;
    [SerializeField] private float phaseOneSpeed = 8f;
    [SerializeField] private float rotationSpeed = 3f;
    [SerializeField] private float knockbackSpeed = 12f;
    [SerializeField] private float maxNbHits = 40f;
    [SerializeField] private float damage = 0.2f;

    [Header("References")]
    [SerializeField] private Transform leftLimit;
    [SerializeField] private Transform rightLimit;
    [SerializeField] private Transform centerLimit;
    [SerializeField] private Transform enemy;
    [SerializeField] private GameObject mainDoor;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private AudioClip hitPlayer;
    [SerializeField] private AudioClip explosion;
    [SerializeField] private AudioClip gotHit;
    [SerializeField] private MusicManager musicManager;
    [SerializeField] private ForcePlayerController playerController;
    [SerializeField] private Portal portal;

    private Vector3 translateVelocity = Vector3.zero;
    private float phaseTransitionTime = 0f;
    private float collisionTimer = 0f;
    private float timeSinceHit = 0f;
    private float deathTimer = 0f;
    private bool isFightStarted = false;
    private bool hasHitPlayer = false;
    private bool isWeak = false;
    private bool isDead = false;
    private bool isDeathExplosionDone = false;
    private int phase = 0;
    private int nbHits = 0;
    private int[] phaseQueue = new int[3] { 1, 2, 3 };

    // Phase One attributes
    private int nbTranslations = 0;
    private Transform initialLimit;
    private Transform otherLimit;

    // Phase Two attributes
    private float chaseTimer = 0f;
    private bool hasJumped = false;

    // Phase Two attributes
    private float weakTimer = 0f;

    private Renderer enemyRenderer;
    private AudioSource audioSource;
    private Transform boss;
    private Rigidbody rb;
    private Animator anim;

    public override void GetHit()
    {
        if (isWeak)
        {
            boss.Find("HitParticles").GetComponent<ParticleSystem>().Emit(200);
            audioSource.PlayOneShot(gotHit);
            nbHits++;
        }
    }

    public void Reset()
    {
        isFightStarted = false;
        isWeak = false;
        hasJumped = false;
        hasHitPlayer = false;
        CanStartFight = false;
        phaseQueue = new int[3] { 1, 2, 3 };
        phase = 0;
        nbHits = 0;
        nbTranslations = 0;
        chaseTimer = 0f;
        collisionTimer = 0f;
        timeSinceHit = 0f;
        phaseTransitionTime = 0f;
        weakTimer = 0f;

        mainDoor.SetActive(false);
        playerController.CanMove = true;
    }

    private void Start()
    {
        enemyRenderer = enemy.GetComponent<Renderer>();
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInParent<Animator>();
        audioSource = GetComponentInParent<AudioSource>();
        boss = enemy.parent;
    }

    private void Update()
    {
        if (!isFightStarted)
        {
            if (enemyRenderer.isVisible && CanStartFight)
            {
                isFightStarted = true;
                musicManager.PlayInitialSoundtrack();
                mainDoor.SetActive(true);
            }
            return;
        }

        HandleDamage();
        HandleRecoil(Time.deltaTime);
        HandleAnimations();
        if (phase == 0)
        {
            if (phaseQueue.Length == 0)
            {
                phaseQueue = new int[3] { 1, 2, 3 };
                isWeak = true;
                phase = 4;
            }

            if (!isWeak)
            {
                if (phaseTransitionTime > 3f)
                {
                    // Decide phase
                    phase = phaseQueue[Random.Range(0, phaseQueue.Length)];
                    phaseQueue = phaseQueue.Where(n => n != phase).ToArray();
                    phaseTransitionTime = 0f;
                }
                else
                {
                    phaseTransitionTime += Time.deltaTime;
                }
            }

            if (nbHits >= maxNbHits)
            {
                phase = 4;
            }
        }

        collisionTimer += Time.deltaTime;
        switch (phase)
        {
            case 1:
                PhaseOne();
                break;
            case 2:
                PhaseTwo();
                break;
            case 3:
                PhaseThree();
                break;
            case 4:
                WeakPhase();
                break;
        }
    }

    private void PhaseOne()
    {
        if (!initialLimit)
        {
            var isLeftLimit = Vector3.Distance(boss.position, leftLimit.position) < Vector3.Distance(boss.position, rightLimit.position);
            initialLimit = isLeftLimit ? leftLimit : rightLimit;
            otherLimit = isLeftLimit ? rightLimit : leftLimit;
        }

        var currentLimit = nbTranslations % 2 == 0 ? initialLimit : otherLimit;
        var destination = MoveTowards(currentLimit.position, phaseOneSpeed, true);

        if (Vector3.Distance(destination, boss.position) < 0.1f)
        {
            nbTranslations++;
            if (nbTranslations == 4)
            {
                phase = 0;
                nbTranslations = 0;
                initialLimit = null;
                otherLimit = null;
            }
        }
    }

    private void PhaseTwo()
    {
        chaseTimer += Time.deltaTime;

        var destination = MoveTowards(playerController.transform.position, runSpeed, !hasJumped);

        if (!hasJumped && (Vector3.Distance(destination, boss.position) < 3f || chaseTimer > 8f))
        {
            rb.velocity = new Vector3(0f, 5f, 0f);
            hasJumped = true;
        }

        if (hasJumped && Mathf.Abs(boss.position.y - leftLimit.position.y) < 0.05f && rb.velocity.y < 0)
        {
            var sphere = boss.Find("ExplosionReference");
            sphere.gameObject.SetActive(true);

            // Use radius and enemy/sphere scales
            var radius = sphere.lossyScale.x / 2;
            var isHit = Physics.CheckSphere(boss.position, radius, playerLayer);
            if (isHit)
            {
                GiveKnockBack(playerController.gameObject.GetComponent<Rigidbody>());
                ApplyDamageToPlayer(playerController.gameObject.GetComponent<Oxygen>(), 2);
            }

            audioSource.PlayOneShot(explosion);
            boss.Find("ExplosionParticles").GetComponent<ParticleSystem>().Emit(500);
            sphere.gameObject.SetActive(false);

            chaseTimer = 0f;
            hasJumped = false;
            initialLimit = null;
            phase = 0;
        }
    }

    private void PhaseThree()
    {
        if (Vector3.Distance(boss.position, centerLimit.position) > 0.05f)
        {
            MoveTowards(centerLimit.position, phaseOneSpeed, true);
            return;
        }

        Quaternion rotation = Quaternion.Euler(0f, -180f, 0);
        boss.rotation = Quaternion.Slerp(boss.rotation, rotation, Time.deltaTime * rotationSpeed);

        var hitboxesContainer = boss.Find("LaserReferences");
        hitboxesContainer.gameObject.SetActive(true);

        var isHit = false;
        foreach (Transform child in hitboxesContainer)
        {
            isHit = isHit || Physics.CheckBox
            (
                child.position,
                child.lossyScale / 2,
                child.rotation,
                playerLayer
            );
        }

        hitboxesContainer.gameObject.SetActive(false);

        if (Mathf.Abs(180f - boss.rotation.eulerAngles.y) > 1f)
        {
            return;
        }

        if (isHit)
        {
            GiveKnockBack(playerController.gameObject.GetComponent<Rigidbody>());
            ApplyDamageToPlayer(playerController.gameObject.GetComponent<Oxygen>(), 2);
        }

        audioSource.PlayOneShot(explosion);
        foreach (Transform laserTransform in boss.Find("LaserParticlesContainer"))
        {
            laserTransform.GetComponent<ParticleSystem>().Emit(300);
        }
        phase = 0;
    }

    private void WeakPhase()
    {
        weakTimer += Time.deltaTime;
        if (weakTimer > 5f)
        {
            phase = 0;
            weakTimer = 0f;
            isWeak = false;
        }

        Vector3 lookPos = (playerController.transform.position - Vector3.forward * 0.1f) - boss.position;
        lookPos.y = 0;

        Quaternion rotation = Quaternion.LookRotation(lookPos);
        boss.rotation = Quaternion.Slerp(boss.rotation, rotation, Time.deltaTime * rotationSpeed * 2f);
    }

    private Vector3 MoveTowards(Vector3 position, float speed, bool moveCondition)
    {
        Vector3 lookPos = (position - Vector3.forward * 0.1f) - boss.position;
        lookPos.y = 0;

        var angle = Quaternion.FromToRotation(boss.forward, lookPos).eulerAngles.y;
        angle = Mathf.Abs(angle > 180 ? angle - 360 : angle);

        Quaternion rotation = Quaternion.LookRotation(lookPos);
        boss.rotation = Quaternion.Slerp(boss.rotation, rotation, Time.deltaTime * rotationSpeed);

        var destination = position;
        destination.y = boss.position.y;

        // Follow player if it is looking at it
        if (angle < 20 && moveCondition)
        {
            boss.position = Vector3.SmoothDamp
            (
                boss.position,
                destination,
                ref translateVelocity,
                0.1f,
                speed,
                Time.deltaTime
            );
        }

        return destination;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collisionTimer < 0.3f)
        {
            return;
        }

        var other = collision.collider;
        Rigidbody player = other.gameObject.GetComponent<Rigidbody>();

        if (player == null)
        {
            return;
        }

        GiveKnockBack(player);

        Oxygen oxygen = other.GetComponent<Oxygen>();
        ApplyDamageToPlayer(oxygen);
        collisionTimer = 0f;
    }

    private void GiveKnockBack(Rigidbody player)
    {
        Vector3 knockbackDirection = Vector3.Normalize(player.position - boss.position);

        player.velocity = knockbackDirection * knockbackSpeed;
        timeSinceHit = 0f;
        playerController.CanMove = false;
        hasHitPlayer = true;
    }

    private void ApplyDamageToPlayer(Oxygen oxygen, float multiplier = 1)
    {
        if (oxygen != null)
        {
            oxygen.DecreaseOxygen(damage * multiplier);
            if (oxygen.OxygenValue <= 0)
            {
                audioSource.PlayOneShot(hitPlayer);
            }
            else
            {
                audioSource.PlayOneShot(hitPlayer);
            }
        }
    }

    private void HandleRecoil(float deltaTime)
    {
        timeSinceHit += deltaTime;
        if (hasHitPlayer && timeSinceHit > 0.5f)
        {
            playerController.CanMove = true;
            hasHitPlayer = false;
        }
    }

    private void HandleAnimations()
    {
        anim.SetBool("IsWeak", isWeak);
        anim.SetBool("IsDead", isDead);
    }

    private void HandleDamage()
    {
        if (isDead)
        {
            deathTimer += Time.deltaTime;
        }

        if (nbHits >= maxNbHits)
        {
            Die();
        }
    }

    private void Die()
    { 
        if (deathTimer > 4)
        {
            transform.gameObject.SetActive(false);
            portal.Activate();
        }
        else if (deathTimer > 2f)
        {
            boss.Find("Slime").gameObject.SetActive(false);
        }
        else if (deathTimer > 1.7f && !isDeathExplosionDone)
        {
            boss.Find("DeathExplosion").GetComponent<ParticleSystem>().Emit(200);
            audioSource.PlayOneShot(explosion);
            boss.Find("Slime").gameObject.SetActive(false);
            isDeathExplosionDone = true;
        }
        if (deathTimer == 0)
        {
            isDead = true;
            boss.Find("DeathParticles").GetComponent<ParticleSystem>().Emit(200);
        }
    }
}
