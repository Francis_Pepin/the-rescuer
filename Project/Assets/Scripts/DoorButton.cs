using UnityEngine;

public class DoorButton : MonoBehaviour
{
    [SerializeField] private float buttonDownSpeed = 1f;
    [SerializeField] private float buttonUpSpeed = 5f;
    [SerializeField] private GameObject button, doorAxis, door;
    [SerializeField] private Transform player;

    private float pressedPercentage;
    private float maxDistance;
    private bool pressed;
    private bool completed;

    private Vector3 initialButtonPos;
    private Quaternion initialDoorRotation;
    private Vector3 lowestButtonPos;

    private Outline buttonOutline;
    private Outline doorOutline;
    private Color off, on, done;

    private void Start()
    {
        pressedPercentage = 0f;

        buttonOutline = button.GetComponent<Outline>();
        doorOutline = door.GetComponent<Outline>();

        off = Color.red;
        on = Color.yellow;
        done = Color.green;

        ChangeMaterial(off);

        initialButtonPos = button.transform.position;
        maxDistance = button.transform.localScale.y * 1.75f;
        lowestButtonPos = initialButtonPos - Vector3.up * maxDistance;

        initialDoorRotation = doorAxis.transform.rotation;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!completed)
        {
            pressed = true;
            ChangeMaterial(on);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!completed)
        {
            pressed = false;
            ChangeMaterial(off);
        }
    }

    private void ChangeMaterial(Color color)
    {
        buttonOutline.OutlineColor = color;
        doorOutline.OutlineColor = color;
    }

    private void Update()
    {
        pressedPercentage = (initialButtonPos.y - button.transform.position.y) / maxDistance;

        doorAxis.transform.rotation = initialDoorRotation;
        doorAxis.transform.Rotate(-Vector3.forward, 90f * pressedPercentage);

        if (pressed && !completed)
        {
            if (pressedPercentage < 1f)
            {
                button.transform.position -= Vector3.up * buttonDownSpeed * Time.deltaTime;
                player.position -= Vector3.up * buttonDownSpeed * Time.deltaTime;
            }
            else
            {
                button.transform.position = lowestButtonPos;
                completed = true;
                ChangeMaterial(done);
            }
        }
        else if (!completed)
        {
            if (pressedPercentage > 0f)
            {
                button.transform.position += Vector3.up * buttonUpSpeed * Time.deltaTime;
            }
            else
            {
                button.transform.position = initialButtonPos;
            }
        }
    }
}
