using UnityEngine;

public class RegenOxygen : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float regenTime = 10f;

    [Header("References")]
    [SerializeField] private Transform capsule;
    [SerializeField] private Transform[] spawnPoints;

    private float regenTimer = 0f;

    private void Update()
    {
        if (spawnPoints.Length == 0)
        {
            return;
        }

        if (!capsule.gameObject.activeSelf)
        {
            if (regenTimer < regenTime)
            {
                regenTimer += Time.deltaTime;
            }
            else
            {
                capsule.gameObject.SetActive(true);
                var spawn = spawnPoints[Random.Range(0, spawnPoints.Length)].position;

                capsule.parent.position = spawn;
                regenTimer = 0f;
            }
        }
    }
}
