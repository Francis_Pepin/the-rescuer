using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private bool isAudioFade = false;
    [SerializeField] private float camSize = 5f;

    [Header("References")]
    [SerializeField] private CheckpointManager checkpointManager;
    [SerializeField] private Color backgroundColor;
    [SerializeField] private AudioClip song;
    [SerializeField] private Camera cam;

    private float timeSinceBackgroundChange = 0f;

    private void Update()
    {
        timeSinceBackgroundChange += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ForcePlayerController>())
        {
            checkpointManager.OnChildTriggerEnter(transform);
            if (backgroundColor != null && timeSinceBackgroundChange > 0.5f)
            {
                checkpointManager.ChangeBackgroundColor(backgroundColor);
            }

            if (song != null)
            {
                checkpointManager.ChangeMusic(song);
            }

            if (isAudioFade)
            {
                checkpointManager.FadeAudio();
            }

            if (cam.orthographicSize != camSize)
            {
                cam.orthographicSize = camSize;
            }
        }
    }
}
