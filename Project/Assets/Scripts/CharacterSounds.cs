using UnityEngine;

public class CharacterSounds : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private AudioClip[] footSteps;
    [SerializeField] private AudioClip[] landings;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioSource jetpackSource;
    [SerializeField] private JetPack jetpack;

    private float timeSinceJetpackStop = 0f;

    private void Step()
    {
        PlayFromList(footSteps);
    }

    private void Land()
    {
        PlayFromList(landings);
    }

    private void PlayFromList(AudioClip[] list)
    {
        AudioClip clip = list[Random.Range(0, list.Length)];
        audioSource.PlayOneShot(clip);
    }

    private void Update()
    {
        timeSinceJetpackStop += Time.deltaTime;
        if (jetpack.IsUsingJetpack && !jetpackSource.isPlaying)
        {
            jetpackSource.Play();
        }
        else if (!jetpack.IsUsingJetpack && jetpackSource.isPlaying)
        {
            jetpackSource.Stop();
        }
    }
}
