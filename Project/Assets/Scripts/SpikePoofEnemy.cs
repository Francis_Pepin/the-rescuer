using UnityEngine;

public class SpikePoofEnemy : Enemy
{
    [Header("Values")]
    [SerializeField] private float moveSpeed = 4f;
    [SerializeField] private float rotationSpeed = 3f;
    [SerializeField] private float detectionRange = 6f;
    [SerializeField] private float damage = 0.2f;
    [SerializeField] private float knockbackSpeed = 8f;
    [SerializeField] private float respawnTime = 5f;

    [Header("References")]
    [SerializeField] private GameObject target;
    [SerializeField] private Transform enemy;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private AudioClip hitPlayer;
    [SerializeField] private AudioClip death;

    private Vector3 translateVelocity = Vector3.zero;
    private float timeSinceHit = 0f;
    private float respawnTimer = 0f;
    private bool hasHitPlayer = false;
    private bool isStunned = false;
    private bool isWallInFront = false;

    private ForcePlayerController playerController;
    private Renderer enemyRenderer;
    private Vector3 spawn;
    private ParticleSystem particles;
    private AudioSource audioSource;
    private Transform spikePoof;
    private Animator anim;

    void Start()
    {
        enemyRenderer = enemy.GetComponent<Renderer>();
        spawn = enemy.position;
        particles = GetComponentInParent<ParticleSystem>();
        anim = GetComponentInParent<Animator>();
        audioSource = GetComponentInParent<AudioSource>();
        playerController = target.GetComponent<ForcePlayerController>();
        spikePoof = enemy.parent;
    }

    void Update()
    {
        respawnTimer += Time.deltaTime;
        if (!enemy.gameObject.activeSelf && respawnTimer > respawnTime)
        {
            enemy.gameObject.SetActive(true);
            enemy.position = spawn;
            var colliders = transform.GetComponents<Collider>();
            foreach (var collider in colliders)
            {
                collider.enabled = true;
            }
        }

        var distanceToPlayer = Vector3.Distance(target.transform.position, spikePoof.position);
        Vector3 lookPos = (target.transform.position - Vector3.forward * 0.1f) - spikePoof.position;
        lookPos.y = 0;

        var angle = Quaternion.FromToRotation(spikePoof.forward, lookPos).eulerAngles.y;
        angle = Mathf.Abs(angle > 180 ? angle - 360 : angle);
        HandleRecoil(Time.deltaTime);

        // If victory or dizzy animation is played, restrict movement
        isStunned = anim.GetCurrentAnimatorStateInfo(0).IsName("Dizzy");
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Victory") || isStunned)
        {
            anim.ResetTrigger("IsStunned");
            return;
        }
        
        var isAtSameHeight = target.transform.position.y + 1 - transform.position.y > 0;
        HandleAnimations(distanceToPlayer, angle, isAtSameHeight);

        // Skip movement if out of range
        if (distanceToPlayer > detectionRange || hasHitPlayer || !enemyRenderer.isVisible || !isAtSameHeight)
        {
            return;
        }

        Quaternion rotation = Quaternion.LookRotation(lookPos);
        spikePoof.rotation = Quaternion.Slerp(spikePoof.rotation, rotation, Time.deltaTime * rotationSpeed);

        var destination = target.transform.position;
        destination.y = spikePoof.position.y;

        isWallInFront = Physics.Raycast(transform.position, -transform.up, 0.4f, whatIsGround);
        // Follow player if it is looking at it
        if (angle < 20 && !isWallInFront)
        {
            spikePoof.position = Vector3.SmoothDamp(
                spikePoof.position,
                destination,
                ref translateVelocity,
                0.1f,
                moveSpeed,
                Time.deltaTime
            );
        }
    }

    public override void GetHit()
    {
        anim.SetTrigger("IsStunned");
    }

    private void OnCollisionEnter(Collision collision)
    {
        var other = collision.collider;
        Rigidbody player = other.gameObject.GetComponent<Rigidbody>();

        if (player == null)
        {
            return;
        }

        Vector3 playerPosition = other.gameObject.transform.position;
        Vector3 knockbackDirection = Vector3.Normalize(playerPosition - spikePoof.position);

        player.velocity = knockbackDirection * knockbackSpeed;
        if (isStunned)
        {
            particles.Play();
            audioSource.PlayOneShot(death);
            enemy.gameObject.SetActive(false);
            respawnTimer = 0f;
            var colliders = transform.GetComponents<Collider>();
            foreach (var collider in colliders)
            {
                collider.enabled = false;
            }
            return;
        }

        timeSinceHit = 0f;
        hasHitPlayer = true;
        playerController.CanMove = false;

        Oxygen oxygen = other.GetComponent<Oxygen>();

        if (oxygen != null)
        {
            oxygen.DecreaseOxygen(damage);
            if (oxygen.OxygenValue <= 0)
            {
                audioSource.PlayOneShot(hitPlayer);
            }
            else
            {
                audioSource.PlayOneShot(hitPlayer);
            }
        }
    }

    private void HandleRecoil(float deltaTime)
    {
        timeSinceHit += deltaTime;
        if (hasHitPlayer && timeSinceHit > 0.5f)
        {
            playerController.CanMove = true;
            hasHitPlayer = false;
        }
    }

    private void HandleAnimations(float distanceToPlayer, float angle, bool isAtSameHeight)
    {
        var isInValidRange = isAtSameHeight && !isWallInFront;
        var isFollowingPlayer = distanceToPlayer < detectionRange && isInValidRange;
        var isSensingPlayer = distanceToPlayer < detectionRange + 2f && !isFollowingPlayer && isInValidRange;
        var isRotating = angle > 20f;
        var isIdle = (!isFollowingPlayer && !isSensingPlayer) || isWallInFront;

        anim.SetBool("IsIdle", isIdle);
        anim.SetBool("IsFollowingPlayer", isFollowingPlayer);
        anim.SetBool("IsRotating", isRotating);
        anim.SetBool("IsSensingPlayer", isSensingPlayer);
        anim.SetBool("HasHitPlayer", hasHitPlayer);
    }
}
