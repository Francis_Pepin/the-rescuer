using UnityEngine;
using System.Linq;

public class RespawnManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform[] enemies;
    [SerializeField] private Transform[] oxygenContainers;
    [SerializeField] private Transform boss;
    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnPoint;
    [SerializeField] private GrapplingHook grapplingHook;
    [SerializeField] private Animator fadeAnim;
    [SerializeField] private AnimationClip fadeAnimation;
    [SerializeField] private Camera cam;

    private float fadeTimer = 0f;
    private bool hasRespawned = false;

    private Vector3[] initialOxygenPositions;
    private Vector3[] initialEnemyPositions;
    private Quaternion[] initialEnemyRotations;
    private Vector3 initialBossPosition;
    private Quaternion initialBossRotation;

    public void HandleRespawn()
    {
        fadeTimer = 0f;
        fadeAnim.SetTrigger("DeathTrigger");
        hasRespawned = false;
    }

    private void Start()
    {
        initialEnemyPositions = enemies.Select(e => e.position).ToArray();
        initialEnemyRotations = enemies.Select(e => e.rotation).ToArray();
        initialOxygenPositions = oxygenContainers.Select(o => o.position).ToArray();
        initialBossPosition = boss.position;
        initialBossRotation = boss.rotation;
    }

    private void Update()
    {
        if (fadeAnim.GetCurrentAnimatorStateInfo(0).IsName("Death Fade"))
        {
            if (fadeTimer >= fadeAnimation.length / 2f && !hasRespawned)
            {
                RespawnAssets();
                hasRespawned = true;
            }
            fadeTimer += Time.deltaTime;
        }
    }

    private void RespawnAssets()
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].Find("TurtleShell").gameObject.SetActive(true);
            enemies[i].position = initialEnemyPositions[i];
            enemies[i].rotation = initialEnemyRotations[i];
        }

        for (int i = 0; i < oxygenContainers.Length; i++)
        {
            oxygenContainers[i].gameObject.SetActive(true);
            oxygenContainers[i].GetChild(0).gameObject.SetActive(true);
            oxygenContainers[i].position = initialOxygenPositions[i];
        }

        boss.position = initialBossPosition;
        boss.rotation = initialBossRotation;
        boss.GetComponent<BossEnemy>().Reset();
        grapplingHook.Reset();
        cam.orthographicSize = 5f;

        player.position = respawnPoint.position;
    }
}
