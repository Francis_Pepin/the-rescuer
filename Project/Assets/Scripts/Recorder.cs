using TMPro;
using UnityEngine;

public class Recorder : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float distance = 3;

    [Header("References")]
    [SerializeField] private TextMeshPro[] texts;
    [SerializeField] private TextMeshPro keycap;
    [SerializeField] private TextMeshPro count;
    [SerializeField] private Transform player;
    [SerializeField] private Oxygen oxygen;

    private int index = -1;
    private bool isAway = true;
    private bool isDecreasingOxygen = true;
    private AudioSource audioSource;
    private float initialDecreaseRate = 0f;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        foreach (var text in texts)
        {
            text.gameObject.SetActive(true);
            text.enabled = false;
        }
        keycap.enabled = true;
        count.gameObject.SetActive(true);
        count.enabled = false;

        initialDecreaseRate = oxygen.linearOxygenDecreasingRatio;
    }

    private void Update()
    {
        if (!isDecreasingOxygen && isAway)
        {
            oxygen.linearOxygenDecreasingRatio = initialDecreaseRate;
            isDecreasingOxygen = true;
        }

        if (Vector3.Distance(player.position, transform.position) > distance)
        {
            if (isAway)
            {
                return;
            }

            index = -1;
            foreach (var text in texts)
            {
                text.enabled = false;
            }
            keycap.enabled = true;
            count.enabled = false;
            isAway = true;
            return;
        }
        else
        {
            isAway = false;
        }

        if (Input.GetButtonDown("Interact"))
        {
            oxygen.linearOxygenDecreasingRatio = 0f;
            isDecreasingOxygen = false;
            audioSource.Play();
            keycap.enabled = false;
            count.enabled = true;
            if (texts.Length > 1)
            {
                index++;
                index %= texts.Length;
                texts[index].enabled = true;

                var previousIndex = (index - 1) % texts.Length;
                if (previousIndex == -1)
                {
                    previousIndex = texts.Length - 1;
                }
                texts[previousIndex].enabled = false;
            }
            else
            {
                texts[0].enabled = true;
                count.text = "1/1";
                return;
            }

            count.text = (index + 1).ToString() + "/" + (texts.Length).ToString();
        }
    }
}
