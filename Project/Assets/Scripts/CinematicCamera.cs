using UnityEngine;
using UnityEngine.UI;

public class CinematicCamera : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float rotateSpeed = 15;

    [Header("References")]
    [SerializeField] private Transform[] planets;
    [SerializeField] private Transform rocketShip;
    [SerializeField] private Transform astronaut;

    private Vector3 distance = new Vector3(0f, 0f, -5f);
    private int spaceShipIndex = 0;
    private float angle = 0f;

    private Animator anim;

    void Start()
    {
        var spaceShipPosition = planets[0].Find("RocketShipPosition");
        var astronautPosition = planets[0].Find("AstronautPosition");
        transform.position = spaceShipPosition.position + distance;

        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Black Start"))
        {
            return;
        }

        var spaceShipPosition = planets[spaceShipIndex].Find("RocketShipPosition");
        var astronautPosition = planets[spaceShipIndex].Find("AstronautPosition");

        if (angle >= 350f)
        {
            anim.SetTrigger("FadeTrigger");
        }

        if (angle >= 360f)
        {
            spaceShipIndex = (spaceShipIndex + 1) % planets.Length;
            spaceShipPosition = planets[spaceShipIndex].Find("RocketShipPosition");
            astronautPosition = planets[spaceShipIndex].Find("AstronautPosition");

            rocketShip.position = spaceShipPosition.position;
            astronaut.position = astronautPosition.position;
            transform.position = spaceShipPosition.position + distance;
            transform.rotation = Quaternion.Euler(10f, 0f, 0f);
            angle = 0f;

            anim.ResetTrigger("FadeTrigger");
        }

        transform.RotateAround(spaceShipPosition.position, Vector3.up, Time.deltaTime * rotateSpeed);
        angle += Time.deltaTime * rotateSpeed;
    }
}
