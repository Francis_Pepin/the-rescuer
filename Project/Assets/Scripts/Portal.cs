using UnityEngine;

public class Portal : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float moveSpeed = 1f;

    public Transform startPosition;
    public Transform endPosition;
    public Transform teleportPosition;
    public Rigidbody player;
    [SerializeField] private GameObject portalPrefab;

    private GameObject spawnedPortal;
    private bool isSpawned = false;

    public void Activate()
    {
        if (!isSpawned)
        {
            isSpawned = true;
            spawnedPortal = Instantiate(portalPrefab);
            spawnedPortal.transform.position = startPosition.position;
        }
    }

    private void Update()
    {
        if (isSpawned)
        {
            spawnedPortal.transform.position = Vector3.Lerp(spawnedPortal.transform.position, endPosition.position, moveSpeed * Time.deltaTime);

            if (Vector3.Distance(spawnedPortal.transform.position, endPosition.position) <= 1f &&
                Vector3.Distance(spawnedPortal.transform.position, player.transform.position) <= 1f)
            {
                player.velocity = Vector3.zero;
                player.transform.position = teleportPosition.position;
            }
        }
    }
}
