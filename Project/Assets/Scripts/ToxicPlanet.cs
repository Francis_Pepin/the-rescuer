using UnityEngine;
using UnityEngine.UI;

public class ToxicPlanet : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform coat;
    [SerializeField] private Transform core;
    [SerializeField] private float rotationSpeed;

    private void Update()
    {
        var rotation = new Vector3(0f, 1f, 0f) * rotationSpeed * Time.deltaTime;
        coat.Rotate(rotation);
        core.Rotate(-rotation);
    }
}
