using UnityEngine;

public class UnlockJetpack : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var jetpack = other.transform.parent.GetComponentInChildren<JetPack>();
        if (jetpack != null)
        {
            jetpack.ProgressBar.SetActive(true);
            jetpack.IsJetpackUnlocked = true;
            gameObject.SetActive(false);
        }
    }
}
