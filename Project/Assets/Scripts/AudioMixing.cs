using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMixing : MonoBehaviour
{

    [SerializeField] private AudioMixer mixer;

    public void SetSoundEffectsVolume(float volume)
    {
        mixer.SetFloat("soundEffectsVol", volume);
    }

    public void SetMusicVolume(float volume)
    {
        mixer.SetFloat("musicVol", volume);
    }
}
