using UnityEngine;

public class ForcePlayerController : MonoBehaviour
{
    private const float SURFACE_DISTANCE = 0.01f;
    private const float IDLE_SPEED = 0.2f;
    private static Vector3 Normal;

    [Header("Values")]
    public bool IsGrounded = true;
    public bool IsInAtmosphere = false;
    public bool CanMove = true;
    public bool IsBeingTransported = false;
    [SerializeField] private float runSpeed = 5f;
    [SerializeField] private float jumpSpeed = 5f;

    // Maximum amount of seconds for the hold jump to be accounted for
    [SerializeField] private float highJumpMaxHold = 0.4f;
    [SerializeField] private float surfaceFriction = 1.2f;
    [SerializeField] private float airFriction = 0.6f;
    [SerializeField] private float accelerationFactor = 400f;

    [Header("References")]
    [SerializeField] private Transform astronautUpAxis;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private Transform ceilingCheck;
    [SerializeField] private LayerMask surfaceMask;
    [SerializeField] private JetPack jetPack;

    private float timeSinceJump = 0f;
    private bool hasPressedJump = false;
    private bool hasReleasedJump = false;

    private Rigidbody rb;
    private Animator anim;

    // Determines if the horizontal velocity is higher than the set buffer
    private bool IsMoving
    {
        get => Mathf.Abs(astronautUpAxis.InverseTransformDirection(rb.velocity).z) > IDLE_SPEED;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = gameObject.GetComponentInChildren<Animator>();
        ForcePlayerController.Normal = new Vector3(0f, 1f, 0f);
    }

    void Update()
    {
        IsGrounded = Physics.CheckSphere(groundCheck.position, SURFACE_DISTANCE, surfaceMask);

        var isCeilingHit = Physics.CheckSphere(ceilingCheck.position, SURFACE_DISTANCE, surfaceMask);
        if (isCeilingHit)
        {
            DepleteHoldJump();
        }

        float input = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0f);

        StorePlayerAngle();
        MovePlayer(input);
        ApplyFriction(input);
        HandleJump();
        if (jetPack != null)
        {
            jetPack.UpdateJetPack(CanMove, IsGrounded);
        }
        HandleAnimations();
    }

    void FixedUpdate()
    {
        HandlePlayerDirection();
    }

    private void StorePlayerAngle()
    {
        // Store astronaut's angle in transform make left/right movement independant from player rotation
        var origin = Vector3.zero;
        var angle = Mathf.Atan2(transform.up.y - origin.y, transform.up.x - origin.x) * 180 / Mathf.PI;

        astronautUpAxis.position = transform.position;
        astronautUpAxis.eulerAngles = new Vector3(90 - angle, 90f, 0f);
    }

    private void MovePlayer(float input)
    {
        // Applies a force to move the player in the relative input direction
        if (input != 0f && CanMove)
        {
            Vector3 locVel = astronautUpAxis.InverseTransformDirection(rb.velocity);
            float effectiveForce = (input * runSpeed - locVel.z) * Time.deltaTime * accelerationFactor;

            Vector3 localSpeed = new Vector3(0f, 0f, effectiveForce);
            Vector3 worldSpeed = astronautUpAxis.TransformDirection(localSpeed);

            // Doesn't apply speed if the player is going faster than the runSpeed input
            if (Mathf.Abs(locVel.z) > runSpeed && input == Mathf.Sign(locVel.z))
            {
                worldSpeed = Vector3.zero;
            }

            rb.AddForce(worldSpeed);
        }
    }

    private void ApplyFriction(float input)
    {
        // Slows down the player on the ground and in the air if no input is found
        if (input == 0f && IsMoving)
        {
            Vector3 locVel = astronautUpAxis.InverseTransformDirection(rb.velocity);
            float friction = IsGrounded ? surfaceFriction : airFriction;

            // Applied friction in opposite of player relative horizontal speed with air/ground factor
            float slowingForce = Mathf.Sign(locVel.z) * -1 * friction * runSpeed;

            float forceFactor = 100f;
            Vector3 localSpeed = new Vector3(0f, 0f, slowingForce) * Time.deltaTime * forceFactor;
            Vector3 worldSpeed = astronautUpAxis.TransformDirection(localSpeed);

            rb.AddForce(worldSpeed);
        }
        else if (input == 0f && !IsMoving)
        {
            // Cancel relative horizontal speed if it's too low
            var relativeVelocity = transform.InverseTransformDirection(rb.velocity);
            rb.velocity = transform.TransformDirection(new Vector3(0f, relativeVelocity.y, 0f));
        }
    }

    private void HandleJump()
    {
        // Used to reset hold jump timer when landing
        if (IsGrounded)
        {
            hasReleasedJump = false;
        }

        // If the player can't move, remove the ability to hold jump when movement is allowed again
        // If the player got off the ground without initially jumping, no hold jump is allowed
        if (!CanMove || (!IsGrounded && !hasPressedJump))
        {
            DepleteHoldJump();
        }

        // Hold jump
        if (Input.GetButton("Jump") && CanMove)
        {
            hasPressedJump = true;
            timeSinceJump = IsGrounded ? 0f : timeSinceJump + Time.deltaTime;
            if (!hasReleasedJump && timeSinceJump < highJumpMaxHold)
            {
                float jumpVelocity = jumpSpeed + (Mathf.Pow(Mathf.Max(0f, highJumpMaxHold - timeSinceJump), 2) * jumpSpeed / 2f);
                var relativeVelocity = transform.InverseTransformDirection(rb.velocity);
                relativeVelocity.y = jumpVelocity;
                rb.velocity = transform.TransformDirection(relativeVelocity);
            }
        }

        hasReleasedJump = hasReleasedJump || Input.GetButtonUp("Jump");
    }

    private void DepleteHoldJump()
    {
        timeSinceJump = highJumpMaxHold;
    }

    private void HandlePlayerDirection()
    {
        // Turn player around if the velocity is negative compared to the facing direction
        var locVel = transform.InverseTransformDirection(rb.velocity);
        if (Mathf.Sign(locVel.z) == -1 && IsMoving && CanMove)
        {
            transform.Rotate(0f, 180f, 0f);
        }
    }

    private void HandleAnimations()
    {
        anim.SetBool("StartJump", Input.GetButton("Jump"));
        anim.SetFloat("TimeSinceJump", timeSinceJump);
        anim.SetBool("Moving", IsMoving);
        anim.SetBool("Grounded", IsGrounded);
    }
}
